package ua.kharkov.semerkov.knure.BullsAndCows.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import ua.kharkov.semerkov.knure.BullsAndCows.*;
import ua.kharkov.semerkov.knure.BullsAndCows.game.Game;
import ua.kharkov.semerkov.knure.BullsAndCows.game.GameType;
import ua.kharkov.semerkov.knure.BullsAndCows.game.MoveState;
import ua.kharkov.semerkov.knure.BullsAndCows.game.PhoneGame;

import java.util.ArrayList;
import java.util.List;

public class PlayerActivity extends Activity {
    private static GameType gameType;
    private static String number;
    private static String opponentName;
    private int moveNumber;
    private TextView moveNumberTextView;
    private TextView opponentNameTextView;
    private DataAdapter playerDataAdapter;
    private DataAdapter opponentDataAdapter;
    private EditText firstNumberEditText;
    private Button firstNumberAddButton;
    private Button firstNumberSubButton;
    private EditText secondNumberEditText;
    private Button secondNumberAddButton;
    private Button secondNumberSubButton;
    private EditText thirdNumberEditText;
    private Button thirdNumberAddButton;
    private Button thirdNumberSubButton;
    private EditText fourthNumberEditText;
    private Button fourthNumberAddButton;
    private Button fourthNumberSubButton;
    private Button guessButton;
    private GridView playerStatGridView;
    private GridView opponentStatGridView;
    private Game game;

    public static GameType getGameType() {
        return gameType;
    }

    public static void setGameType(GameType gameType) {
        PlayerActivity.gameType = gameType;
    }

    public static String getNumber() {
        return number;
    }

    public static void setNumber(String number) {
        PlayerActivity.number = number;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player);
        moveNumberTextView = (TextView) findViewById(R.id.moveNumberTextView);
        moveNumberTextView.setText("Moves: " + moveNumber);
        firstNumberEditText = (EditText) findViewById(R.id.firstNumberEditText);
        secondNumberEditText = (EditText) findViewById(R.id.secondNumberEditText);
        thirdNumberEditText = (EditText) findViewById(R.id.thirdNumberEditText);
        fourthNumberEditText = (EditText) findViewById(R.id.fourthNumberEditText);
        initNumberButtons();
        final GridView playerGridView = (GridView) findViewById(R.id.playerGridView);
        final GridView opponentGridView = (GridView) findViewById(R.id.opponentGridView);
        opponentNameTextView = (TextView) findViewById(R.id.opponentNameTextView);
        List<String> list = new ArrayList<String>();
        list.add(Constants.UNKNOWN_VALUE);
        list.add(Constants.BULLS);
        list.add(Constants.COWS);
        playerStatGridView = (GridView) findViewById(R.id.playerStatGridView);
        playerStatGridView.setAdapter(new DataAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, list, GridViewType.GRID_VIEW_HEAER));
        playerDataAdapter = new DataAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, GridViewType.GRID_VIEW_TABLE);
        playerGridView.setAdapter(playerDataAdapter);
        String opponentNumber = PlayerActivity.number;
        list.set(0, opponentNumber);
        opponentStatGridView = (GridView) findViewById(R.id.opponentStatGridView);
        opponentStatGridView.setAdapter(new DataAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, list, GridViewType.GRID_VIEW_HEAER));
        opponentDataAdapter = new DataAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, GridViewType.GRID_VIEW_TABLE);
        opponentGridView.setAdapter(opponentDataAdapter);
        guessButton = (Button) findViewById(R.id.guessButton);
        guessButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int firstNumber = firstNumberEditText.getText().charAt(0) - 48;
                int secondNumber = secondNumberEditText.getText().charAt(0) - 48;
                int thirdNumber = thirdNumberEditText.getText().charAt(0) - 48;
                int fourthNumber = fourthNumberEditText.getText().charAt(0) - 48;
                StringBuilder sb = new StringBuilder();
                sb.append(firstNumber);
                sb.append(secondNumber);
                sb.append(thirdNumber);
                sb.append(fourthNumber);
                MoveState moveState = game.checkNumber(sb.toString());
                playerDataAdapter.add(sb.toString());
                playerDataAdapter.add(String.valueOf(moveState.getNumberOfBulls()));
                playerDataAdapter.add(String.valueOf(moveState.getNumberOfCows()));
                playerDataAdapter.notifyDataSetChanged();
                playerGridView.smoothScrollToPosition(playerDataAdapter.getCount() - 1);
                String opponentNumber = game.makeGuess();
                MoveState opponentMoveState = checkNumber(opponentNumber);
                game.addMoveState(opponentMoveState);
                opponentDataAdapter.add(opponentMoveState.getNumber());
                opponentDataAdapter.add(String.valueOf(opponentMoveState.getNumberOfBulls()));
                opponentDataAdapter.add(String.valueOf(opponentMoveState.getNumberOfCows()));
                opponentDataAdapter.notifyDataSetChanged();
                opponentGridView.smoothScrollToPosition(opponentDataAdapter.getCount() - 1);
                moveNumber++;
                moveNumberTextView.setText("Moves: " + moveNumber);
                if (moveState.getNumberOfBulls() == 4 || opponentMoveState.getNumberOfBulls() == 4) {
                    if (moveState.getNumberOfBulls() == 4) {
                        FinishActivity.setHead(Constants.WIN);
                        FinishActivity.setMessage(Constants.NEW_GAME);
                    }
                    if (opponentMoveState.getNumberOfBulls() == 4) {
                        FinishActivity.setHead(Constants.LOSS);
                        FinishActivity.setMessage(Constants.NEW_GAME);
                    }
                    Intent intent = new Intent(getApplicationContext(), FinishActivity.class);
                    startActivity(intent);
                }
            }
        });
        switch (PlayerActivity.gameType) {
            case GAME_WITH_PHONE:
                game = new PhoneGame();
                opponentName = Constants.PHONE;
                break;
        }
        opponentNameTextView.setText(opponentName);
    }

    @Override
    public void onBackPressed(){}

    private void initNumberButtons() {
        firstNumberEditText = (EditText) findViewById(R.id.firstNumberEditText);
        firstNumberAddButton = (Button) findViewById(R.id.firstNumberAddButton);
        firstNumberSubButton = (Button) findViewById(R.id.firstNumberSubButton);
        firstNumberAddButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = firstNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(1);
                int tmpCounter = counter + 1;
                while (tmpCounter < 10) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter++;
                }
                if (tmpCounter != 10) {
                    counter = tmpCounter;
                }
                firstNumberEditText.setText(String.valueOf(counter));
            }
        });
        firstNumberSubButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = firstNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(1);
                int tmpCounter = counter - 1;
                while (tmpCounter > -1) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter--;
                }
                if (tmpCounter != -1) {
                    counter = tmpCounter;
                }
                firstNumberEditText.setText(String.valueOf(counter));
            }
        });
        secondNumberEditText = (EditText) findViewById(R.id.secondNumberEditText);
        secondNumberAddButton = (Button) findViewById(R.id.secondNumberAddButton);
        secondNumberSubButton = (Button) findViewById(R.id.secondNumberSubButton);
        secondNumberAddButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = secondNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(2);
                int tmpCounter = counter + 1;
                while (tmpCounter < 10) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter++;
                }
                if (tmpCounter != 10) {
                    counter = tmpCounter;
                }
                secondNumberEditText.setText(String.valueOf(counter));
            }
        });
        secondNumberSubButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = secondNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(2);
                int tmpCounter = counter - 1;
                while (tmpCounter > -1) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter--;
                }
                if (tmpCounter != -1) {
                    counter = tmpCounter;
                }
                secondNumberEditText.setText(String.valueOf(counter));
            }
        });
        thirdNumberEditText = (EditText) findViewById(R.id.thirdNumberEditText);
        thirdNumberAddButton = (Button) findViewById(R.id.thirdNumberAddButton);
        thirdNumberSubButton = (Button) findViewById(R.id.thirdNumberSubButton);
        thirdNumberAddButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = thirdNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(3);
                int tmpCounter = counter + 1;
                while (tmpCounter < 10) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter++;
                }
                if (tmpCounter != 10) {
                    counter = tmpCounter;
                }
                thirdNumberEditText.setText(String.valueOf(counter));
            }
        });
        thirdNumberSubButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = thirdNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(3);
                int tmpCounter = counter - 1;
                while (tmpCounter > -1) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter--;
                }
                if (tmpCounter != -1) {
                    counter = tmpCounter;
                }
                thirdNumberEditText.setText(String.valueOf(counter));
            }
        });
        fourthNumberEditText = (EditText) findViewById(R.id.fourthNumberEditText);
        fourthNumberAddButton = (Button) findViewById(R.id.fourthNumberAddButton);
        fourthNumberSubButton = (Button) findViewById(R.id.fourthNumberSubButton);
        fourthNumberAddButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = fourthNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(4);
                int tmpCounter = counter + 1;
                while (tmpCounter < 10) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter++;
                }
                if (tmpCounter != 10) {
                    counter = tmpCounter;
                }
                fourthNumberEditText.setText(String.valueOf(counter));
            }
        });
        fourthNumberSubButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = fourthNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(4);
                int tmpCounter = counter - 1;
                while (tmpCounter > -1) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter--;
                }
                if (tmpCounter != -1) {
                    counter = tmpCounter;
                }
                fourthNumberEditText.setText(String.valueOf(counter));
            }
        });
    }

    private List<Integer> getSelectedNumbers(int pos) {
        List<Integer> selectedNumbers = new ArrayList<Integer>(4);
        selectedNumbers.add(firstNumberEditText.getText().charAt(0) - 48);
        selectedNumbers.add(secondNumberEditText.getText().charAt(0) - 48);
        selectedNumbers.add(thirdNumberEditText.getText().charAt(0) - 48);
        selectedNumbers.add(fourthNumberEditText.getText().charAt(0) - 48);
        selectedNumbers.remove(pos - 1);
        return selectedNumbers;
    }

    private MoveState checkNumber(String opponentNumber) {
        int numberOfBulls = 0;
        int numberOfCows = 0;
        for (int i = 0; i < opponentNumber.length(); i++) {
            char ch = number.charAt(i);
            int pos = opponentNumber.indexOf(ch);
            if (pos != -1) {
                if (pos == i) {
                    numberOfBulls++;
                } else {
                    numberOfCows++;
                }
            }
        }
        MoveState moveState = new MoveState(opponentNumber, numberOfBulls, numberOfCows);
        return moveState;
    }
}