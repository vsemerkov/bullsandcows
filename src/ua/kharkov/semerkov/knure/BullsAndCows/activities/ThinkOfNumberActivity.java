package ua.kharkov.semerkov.knure.BullsAndCows.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import ua.kharkov.semerkov.knure.BullsAndCows.R;

import java.util.ArrayList;
import java.util.List;

public class ThinkOfNumberActivity extends Activity {
    private EditText firstNumberEditText;
    private Button firstNumberAddButton;
    private Button firstNumberSubButton;
    private EditText secondNumberEditText;
    private Button secondNumberAddButton;
    private Button secondNumberSubButton;
    private EditText thirdNumberEditText;
    private Button thirdNumberAddButton;
    private Button thirdNumberSubButton;
    private EditText fourthNumberEditText;
    private Button fourthNumberAddButton;
    private Button fourthNumberSubButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.think);
        initNumberButtons();
    }

    public void playOnClick(View v) {
        int firstNumber = firstNumberEditText.getText().charAt(0) - 48;
        int secondNumber = secondNumberEditText.getText().charAt(0) - 48;
        int thirdNumber = thirdNumberEditText.getText().charAt(0) - 48;
        int fourthNumber = fourthNumberEditText.getText().charAt(0) - 48;
        StringBuilder sb = new StringBuilder();
        sb.append(firstNumber);
        sb.append(secondNumber);
        sb.append(thirdNumber);
        sb.append(fourthNumber);
        PlayerActivity.setNumber(sb.toString());
        Intent intent = new Intent(this, PlayerActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){}

    private void initNumberButtons() {
        firstNumberEditText = (EditText) findViewById(R.id.thFirstNumberEditText);
        firstNumberAddButton = (Button) findViewById(R.id.thFirstNumberAddButton);
        firstNumberSubButton = (Button) findViewById(R.id.thFirstNumberSubButton);
        firstNumberAddButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = firstNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(1);
                int tmpCounter = counter + 1;
                while (tmpCounter < 10) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter++;
                }
                if (tmpCounter != 10) {
                    counter = tmpCounter;
                }
                firstNumberEditText.setText(String.valueOf(counter));
            }
        });
        firstNumberSubButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = firstNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(1);
                int tmpCounter = counter - 1;
                while (tmpCounter > -1) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter--;
                }
                if (tmpCounter != -1) {
                    counter = tmpCounter;
                }
                firstNumberEditText.setText(String.valueOf(counter));
            }
        });
        secondNumberEditText = (EditText) findViewById(R.id.thSecondNumberEditText);
        secondNumberAddButton = (Button) findViewById(R.id.thSecondNumberAddButton);
        secondNumberSubButton = (Button) findViewById(R.id.thSecondNumberSubButton);
        secondNumberAddButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = secondNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(2);
                int tmpCounter = counter + 1;
                while (tmpCounter < 10) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter++;
                }
                if (tmpCounter != 10) {
                    counter = tmpCounter;
                }
                secondNumberEditText.setText(String.valueOf(counter));
            }
        });
        secondNumberSubButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = secondNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(2);
                int tmpCounter = counter - 1;
                while (tmpCounter > -1) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter--;
                }
                if (tmpCounter != -1) {
                    counter = tmpCounter;
                }
                secondNumberEditText.setText(String.valueOf(counter));
            }
        });
        thirdNumberEditText = (EditText) findViewById(R.id.thThirdNumberEditText);
        thirdNumberAddButton = (Button) findViewById(R.id.thThirdNumberAddButton);
        thirdNumberSubButton = (Button) findViewById(R.id.thThirdNumberSubButton);
        thirdNumberAddButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = thirdNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(3);
                int tmpCounter = counter + 1;
                while (tmpCounter < 10) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter++;
                }
                if (tmpCounter != 10) {
                    counter = tmpCounter;
                }
                thirdNumberEditText.setText(String.valueOf(counter));
            }
        });
        thirdNumberSubButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = thirdNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(3);
                int tmpCounter = counter - 1;
                while (tmpCounter > -1) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter--;
                }
                if (tmpCounter != -1) {
                    counter = tmpCounter;
                }
                thirdNumberEditText.setText(String.valueOf(counter));
            }
        });
        fourthNumberEditText = (EditText) findViewById(R.id.thFourthNumberEditText);
        fourthNumberAddButton = (Button) findViewById(R.id.thFourthNumberAddButton);
        fourthNumberSubButton = (Button) findViewById(R.id.thFourthNumberSubButton);
        fourthNumberAddButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = fourthNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(4);
                int tmpCounter = counter + 1;
                while (tmpCounter < 10) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter++;
                }
                if (tmpCounter != 10) {
                    counter = tmpCounter;
                }
                fourthNumberEditText.setText(String.valueOf(counter));
            }
        });
        fourthNumberSubButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                int counter = fourthNumberEditText.getText().charAt(0) - 48;
                List<Integer> selectedNumbers = getSelectedNumbers(4);
                int tmpCounter = counter - 1;
                while (tmpCounter > -1) {
                    if (!selectedNumbers.contains(tmpCounter)) break;
                    tmpCounter--;
                }
                if (tmpCounter != -1) {
                    counter = tmpCounter;
                }
                fourthNumberEditText.setText(String.valueOf(counter));
            }
        });
    }

    private List<Integer> getSelectedNumbers(int pos) {
        List<Integer> selectedNumbers = new ArrayList<Integer>(4);
        selectedNumbers.add(firstNumberEditText.getText().charAt(0) - 48);
        selectedNumbers.add(secondNumberEditText.getText().charAt(0) - 48);
        selectedNumbers.add(thirdNumberEditText.getText().charAt(0) - 48);
        selectedNumbers.add(fourthNumberEditText.getText().charAt(0) - 48);
        selectedNumbers.remove(pos - 1);
        return selectedNumbers;
    }
}