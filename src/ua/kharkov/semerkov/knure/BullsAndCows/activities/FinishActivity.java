package ua.kharkov.semerkov.knure.BullsAndCows.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import ua.kharkov.semerkov.knure.BullsAndCows.R;

public class FinishActivity extends Activity {
    private static String head;
    private static String message;
    private TextView headTextView;
    private TextView messageTextView;

    public static void setHead(String head) {
        FinishActivity.head = head;
    }

    public static void setMessage(String message) {
        FinishActivity.message = message;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finish);
        headTextView = (TextView) findViewById(R.id.headTextView);
        messageTextView = (TextView) findViewById(R.id.messageTextView);
        headTextView.setText(head);
        messageTextView.setText(message);
    }

    public void okOnClick(View v) {
        Intent intent = new Intent(this, ThinkOfNumberActivity.class);
        startActivity(intent);
    }

    public void cancelOnClick(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){}
}