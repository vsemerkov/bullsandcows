package ua.kharkov.semerkov.knure.BullsAndCows.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import ua.kharkov.semerkov.knure.BullsAndCows.User;
import ua.kharkov.semerkov.knure.BullsAndCows.database.UserDataSource;
import ua.kharkov.semerkov.knure.BullsAndCows.game.GameType;
import ua.kharkov.semerkov.knure.BullsAndCows.R;

import java.net.InetAddress;
import java.util.List;

public class MainActivity extends Activity {
    private UserDataSource datasource;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        datasource = new UserDataSource(this);
        datasource.open();
        User user = datasource.getUser();
        datasource.close();
        if (user == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }

    public void playWithPhoneOnClick(View v) {
        PlayerActivity.setGameType(GameType.GAME_WITH_PHONE);
        Intent intent = new Intent(this, ThinkOfNumberActivity.class);
        startActivity(intent);
    }

    public void signoutOnClick(View v) {
        datasource = new UserDataSource(this);
        datasource.open();
        User user = datasource.getUser();
        datasource.removeUser(user);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void socketGameOnClick(View v) {
    }

    public void exitOnClick(View v) {
        finish();
        System.runFinalizersOnExit(true);
        System.exit(0);
    }

    @Override
    public void onBackPressed() {
    }
}