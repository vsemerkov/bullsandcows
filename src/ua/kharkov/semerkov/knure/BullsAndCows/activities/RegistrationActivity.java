package ua.kharkov.semerkov.knure.BullsAndCows.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import ua.kharkov.semerkov.knure.BullsAndCows.*;
import ua.kharkov.semerkov.knure.BullsAndCows.database.UserDataSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class RegistrationActivity extends Activity {
    private UserDataSource datasource;
    private EditText loginText;
    private EditText passwordText;
    private EditText passwordRepeatText;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        loginText = (EditText) findViewById(R.id.regLoginText);
        passwordText = (EditText) findViewById(R.id.regPasswordText);
        passwordRepeatText = (EditText) findViewById(R.id.regPasswordRepeatText);
    }

    public void regOnClick(View v) throws IOException {
        if (!ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
            showAlertDialog(RegistrationActivity.this, "No Internet connection!",
                    "You don't have internet connection!", false);
            return;
        }
        String login = String.valueOf(loginText.getText());
        String password = String.valueOf(passwordText.getText());
        String passwordRepeat = String.valueOf(passwordRepeatText.getText());
        Validator validator = new Validator();
        Map<String, String> map = validator.validateRegistrationData(login, password, passwordRepeat);
        if (map.size() != 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(map.get(Constants.USER_LOGIN) != null ? map.get(Constants.USER_LOGIN) + " " : "");
            sb.append(map.get(Constants.USER_PASSWORD) != null ? map.get(Constants.USER_PASSWORD) + " " : "");
            sb.append(map.get(Constants.USER_PASSWORD_REPEAT) != null ? map.get(Constants.USER_PASSWORD_REPEAT) : "");
            showAlertDialog(RegistrationActivity.this, "Incorrect data!",
                    sb.toString(), false);
            return;
        }
        User user = new User(login, password);
        user.setPassword(Password.hash(user.getPassword()));
        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost("http://gamebulls.jvmhost.net/GameBulls/signup");
        List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
        postParameters.add(new BasicNameValuePair(Constants.USER_LOGIN, user.getLogin()));
        postParameters.add(new BasicNameValuePair(Constants.USER_PASSWORD, user.getPassword()));
        UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
        request.setEntity(formEntity);
        HttpResponse response = client.execute(request);
        Scanner scanner = new Scanner(response.getEntity().getContent());
        String str = scanner.nextLine();
        if (!str.equals("true")) {
            showAlertDialog(RegistrationActivity.this, "Error!",
                    str, false);
            return;
        }
        datasource = new UserDataSource(this);
        datasource.open();
        datasource.addUser(user);
        datasource.close();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, Constants.OK, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }
}