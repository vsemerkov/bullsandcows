package ua.kharkov.semerkov.knure.BullsAndCows;

public class Constants {
    public static final String BULLS = "bulls";
    public static final String COWS = "cows";
    public static final String UNKNOWN_VALUE = "****";
    public static final String PHONE = "Phone";
    public static final String NEW_GAME = "Do you want to start a new game?";
    public static final String WIN = "Congratulations!";
    public static final String LOSS = "You are losing!";
    public static final String OK = "OK";
    public static final String CANCEL = "CANCEL";
    public static final String USER = "user";
    public static final String USER_LOGIN = "login";
    public static final String USER_PASSWORD = "password";
    public static final String USER_PASSWORD_REPEAT = "passwordRepeat";
}
