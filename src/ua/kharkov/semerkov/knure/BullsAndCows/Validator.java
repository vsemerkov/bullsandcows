package ua.kharkov.semerkov.knure.BullsAndCows;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Validator {
    private Pattern loginPattern = Pattern.compile("^[a-zA-Z\\d]{2,25}$");
    private Pattern passwordPattern = Pattern
            .compile("(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$");

    public Map<String, String> validateRegistrationData(String login, String password, String passwordRepeat) {
        Map<String, String> paramErrors = new LinkedHashMap<String, String>();
        if (!validateLogin(login)) {
            paramErrors.put(Constants.USER_LOGIN,
                    "Login is incorrect!");
        }
        if (!validatePassword(password)) {
            paramErrors
                    .put(Constants.USER_PASSWORD,
                            "Password must contains more than 8 characters, lower-case and upper-case characters, digits, wildcard characters!");
        }
        if (password != null) {
            boolean fl = password.equals(
                    passwordRepeat);
            if (!fl) {
                paramErrors.put(Constants.USER_PASSWORD_REPEAT,
                        "Passwords don't agree!");
            }
        }
        return paramErrors;
    }

    public Map<String, String> validateLoginData(String login, String password) {
        Map<String, String> paramErrors = new LinkedHashMap<String, String>();
        if (!validateLogin(login)) {
            paramErrors.put(Constants.USER_LOGIN,
                    "Login is incorrect!");
        }
        if (!validatePassword(password)) {
            paramErrors
                    .put(Constants.USER_PASSWORD,
                            "Password must contains more than 8 characters, lower-case and upper-case characters, digits, wildcard characters!");
        }
        return paramErrors;
    }

    private boolean checkStringValue(String value, Pattern pattern) {
        if (value != null) {
            return pattern.matcher(value).lookingAt();
        } else {
            return false;
        }
    }

    private boolean validateLogin(String name) {
        return checkStringValue(name, loginPattern);
    }

    private boolean validatePassword(String password) {
        return checkStringValue(password, passwordPattern);
    }
}
