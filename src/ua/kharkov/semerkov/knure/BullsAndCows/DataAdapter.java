package ua.kharkov.semerkov.knure.BullsAndCows;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DataAdapter extends ArrayAdapter<String> {
    private Context context;
    private GridViewType gridViewType;

    public DataAdapter(Context context, int textViewResourceId, List<String> list, GridViewType gridViewType) {
        super(context, textViewResourceId, new ArrayList<String>(list));
        this.gridViewType = gridViewType;
        this.context = context;
    }

    public DataAdapter(Context context, int textViewResourceId, GridViewType gridViewType) {
        this(context, textViewResourceId, new ArrayList<String>(), gridViewType);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) convertView;
        if (convertView == null) {
            convertView = new TextView(context);
            textView = (TextView) convertView;
        }
        textView.setText(getItem(position));
        textView.setTypeface(null, Typeface.BOLD);
        switch (gridViewType) {
            case GRID_VIEW_TABLE:
                textView.setTextColor(Color.BLACK);
                textView.setBackgroundColor(Color.rgb(0xff, 0xcf, 0x1e));
                break;
            case GRID_VIEW_HEAER:
                textView.setTextColor(Color.WHITE);
                textView.setBackgroundColor(Color.rgb(0xa5, 0xa0, 0xa8));
                break;
        }
        return convertView;
    }
}
