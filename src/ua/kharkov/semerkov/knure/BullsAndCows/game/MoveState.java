package ua.kharkov.semerkov.knure.BullsAndCows.game;

public class MoveState {
    private String number;
    private int numberOfBulls;
    private int numberOfCows;

    public MoveState() {
    }

    public MoveState(String number, int numberOfBulls, int numberOfCows) {
        this.number = number;
        this.numberOfBulls = numberOfBulls;
        this.numberOfCows = numberOfCows;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getNumberOfBulls() {
        return numberOfBulls;
    }

    public void setNumberOfBulls(int numberOfBulls) {
        this.numberOfBulls = numberOfBulls;
    }

    public int getNumberOfCows() {
        return numberOfCows;
    }

    public void setNumberOfCows(int numberOfCows) {
        this.numberOfCows = numberOfCows;
    }
}
