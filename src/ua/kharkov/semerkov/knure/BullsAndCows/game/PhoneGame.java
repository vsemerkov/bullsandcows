package ua.kharkov.semerkov.knure.BullsAndCows.game;

import java.util.Random;

public class PhoneGame implements Game {
    private static final String[] TEST_NUMBERS = {"1234", "5678", "9012"};
    private int moveNumber;
    private boolean[] numberFlags;
    private String inventedNumber;

    public PhoneGame() {
        this.inventedNumber = inventNumber();
        numberFlags = new boolean[10000];
        for (int i = 0; i < 10000; i++) {
            numberFlags[i] = true;
        }
    }

    private String inventNumber() {
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder();
        while (sb.length() != 4) {
            int number = rnd.nextInt(10);
            if (sb.indexOf(String.valueOf(number)) == -1) {
                sb.append(number);
            }
        }
        return sb.toString();
    }

    @Override
    public MoveState checkNumber(String number) {
        int numberOfBulls = 0;
        int numberOfCows = 0;
        for (int i = 0; i < number.length(); i++) {
            char ch = inventedNumber.charAt(i);
            int pos = number.indexOf(ch);
            if (pos != -1) {
                if (pos == i) {
                    numberOfBulls++;
                } else {
                    numberOfCows++;
                }
            }
        }
        MoveState moveState = new MoveState(number, numberOfBulls, numberOfCows);
        return moveState;
    }

    @Override
    public String makeGuess() {
        moveNumber++;
        if (moveNumber <= 3) {
            return TEST_NUMBERS[moveNumber - 1];
        }
        int ind = 0;
        for (int i = 0; i < 10000; i++) {
            if (numberFlags[i]) {
                ind = i;
                break;
            }
        }
        return getNumberByIndex(ind);
    }

    @Override
    public void addMoveState(MoveState moveState) {
        if (moveState.getNumberOfBulls() == 4 && moveNumber < 4) {
            moveNumber = 4;
        }
        for (int i = 0; i < 10000; i++) {
            if (!numberFlags[i]) continue;
            String testNumber = getNumberByIndex(i);
            int count = 0;
            for (int j = 0; j < 4; j++) {
                if (moveState.getNumber().charAt(j) == testNumber.charAt(j)) {
                    count++;
                }
            }
            if (count != moveState.getNumberOfBulls()) {
                numberFlags[i] = false;
            }
            count = 0;
            for (int j = 0; j < 4; j++) {
                if ((moveState.getNumber().charAt(j) != testNumber.charAt(j)) && (testNumber.indexOf(moveState.getNumber().charAt(j)) != -1)) {
                    count++;
                }
            }
            if (count < moveState.getNumberOfCows()) {
                numberFlags[i] = false;
            }
        }
    }

    private String getNumberByIndex(int ind) {
        StringBuilder sb = new StringBuilder(4);
        sb.append((ind / 1000) % 10);
        sb.append((ind / 100) % 10);
        sb.append((ind / 10) % 10);
        sb.append(ind % 10);
        return sb.toString();
    }
}
