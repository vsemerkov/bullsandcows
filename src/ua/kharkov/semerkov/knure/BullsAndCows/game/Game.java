package ua.kharkov.semerkov.knure.BullsAndCows.game;

public interface Game {

    public MoveState checkNumber(String number);

    public String makeGuess();

    public void addMoveState(MoveState moveState);
}
