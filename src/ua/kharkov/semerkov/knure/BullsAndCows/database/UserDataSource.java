package ua.kharkov.semerkov.knure.BullsAndCows.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import ua.kharkov.semerkov.knure.BullsAndCows.User;

import java.util.ArrayList;
import java.util.List;

public class UserDataSource {
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {UserTable.USER__ID
            , UserTable.USER__LOGIN
            , UserTable.USER__PASSWORD
    };

    public UserDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public User addUser(User user) {
        ContentValues values = new ContentValues();
        values.put(UserTable.USER__LOGIN, user.getLogin());
        values.put(UserTable.USER__PASSWORD, user.getPassword());
        long insertId = database.insert(UserTable.USER__TABLE_NAME, null,
                values);
        Cursor cursor = database.query(UserTable.USER__TABLE_NAME,
                allColumns, UserTable.USER__ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        User newUser = getUser(cursor);
        cursor.close();
        return newUser;
    }

    public void removeUser(User user) {
        if (user != null) {
            database.delete(UserTable.USER__TABLE_NAME, UserTable.USER__ID
                    + " = " + user.getId(), null);
        }
    }

    public boolean updateUser(User user) {
        ContentValues values = new ContentValues();
        values.put(UserTable.USER__LOGIN, user.getLogin());
        values.put(UserTable.USER__PASSWORD, user.getPassword());
        int updated = database.update(UserTable.USER__TABLE_NAME, values, UserTable.USER__ID + " = " + user.getId(), null);
        return updated > 0;
    }

    public User getUser() {
        User user = null;
        Cursor cursor = database.query(UserTable.USER__TABLE_NAME,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            user = getUser(cursor);
        }
        cursor.close();
        return user;
    }

    private User getUser(Cursor cursor) {
        User user = new User();
        user.setId(cursor.getLong(0));
        user.setLogin(cursor.getString(1));
        user.setPassword(cursor.getString(2));
        return user;
    }
}
