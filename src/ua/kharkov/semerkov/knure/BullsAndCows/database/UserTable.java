package ua.kharkov.semerkov.knure.BullsAndCows.database;

import android.database.sqlite.SQLiteDatabase;

public class UserTable {
    public static final String USER__TABLE_NAME = "users";
    public static final String USER__ID = "id";
    public static final String USER__LOGIN = "login";
    public static final String USER__PASSWORD = "password";
    private static final String DATABASE_CREATE = "CREATE TABLE "
            + USER__TABLE_NAME
            + "("
            + USER__ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + USER__LOGIN + " TEXT NOT NULL, "
            + USER__PASSWORD + " TEXT NOT NULL"
            + ");";

    public static void createNoteTable(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + USER__TABLE_NAME);
        createNoteTable(database);
    }
}
